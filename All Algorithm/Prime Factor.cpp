#include <iostream>
#include <math.h>
#include <cstdio>
#define N 10000000
using namespace std;
int prime[N];
void save();
int main()
{
    int num;
    save();
    while(cin>>num && num!=0){
        while(num%2 == 0){
            cout<<2<<" ";
            num=num/2;
        }
        for(int n=3; n<=num; n+=2){
            if((prime[n]==0) && (num%n==0)){
                while(num%n == 0){
                    cout<<n<<" ";
                    num=num/n;
                }
            }
        }
        cout<<endl;
    }
    return 0;
}
void save()
{
    int root=sqrt(N)+1, sum=1;
    for(int i=3; i<=root; i+=2){
        if(prime[i]==0){
            for(int j=i*i; j<=N; j+=i+i){
                prime[j] = 1;
            }
        }
    }
}
