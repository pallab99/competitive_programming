#include <bits/stdc++.h>
#define N 111111
using namespace std;
bool pri[N+5];
int prime[N], ck[64];
void seive();
int prime_factor(int number);
int main()
{
    freopen("input.txt", "r", stdin);
    seive();
    int a, b, len, lcd[505], x=0, n;
    memset(lcd, 0, sizeof(lcd));
    cin>>n;
    for(int i=0; i<n; i++){
        cin>>a;
        len = prime_factor(a);
        for(int j=0, k=0; j<len; j++){
            int temp = ck[j];
            while(1){
                if(temp==lcd[k]) break;
                else if(lcd[k]==0){
                    lcd[x++]=ck[j];
                    break;
                }
                k++;
            }
        }
    }
    for(int i=0; i<x; i++){
        cout<<lcd[i]<<' ';
    }
    cout<<endl;

    return 0;
}
void seive()
{
    int root = sqrt(N)+1, j, i;
    pri[0]=1, pri[1]=1;
    for(i=3; i<=root; i+=2)if(pri[i]==0){
        for(j=i*i; j<=N; j+=i+i)
            pri[j]=1;
    }
    prime[0]=2;
    for(i=3, j=1; i<=N; i+=2)
        if(pri[i]==0)
            prime[j++]=i;
}
int prime_factor(int number)
{
    int i, j, k;
    memset(ck,0,sizeof(ck));
    for(i=0, j=0; i<number; i++){
        while(number%prime[i]==0){
            ck[j++]=prime[i];
            number/=prime[i];
        }
        if(pri[number]==0){
            ck[j++]=number;
            break;
        }
    }
    return j;
}
