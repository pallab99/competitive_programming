#include <iostream>
#include <cstdio>
#include <algorithm>
#define SIZ 100
using namespace std;
int d[SIZ][SIZ];
int main()
{
    freopen("graph.txt", "r", stdin);
    int x, y, w, n, edge, i, j, k;
    cin>>n>>edge;
    while(edge--){
        cin>>x>>y>>w;
        d[x][y]=w;
    }
    for(k=0; k<n; k++){
        for(i=0; i<n; i++){
            for(j=0; j<n; j++) {
                if(d[i][j] > d[i][k] + d[k][j])
                    d[i][j] = d[i][k] + d[k][j];
            }
        }
    }
    cin>>edge;
    return 0;
}
