#include <bits/stdc++.h>
using namespace std;
#define Size 100
vector<pair< pair<int, int>, int>>graph;
int dis[Size], parents[Size];
bool relax(int u, int v, int w);
bool bellman_ford(int source, vector< pair< pair<int, int>, int> >graph, int V);
int main() {
    freopen("input.txt", "r", stdin);
    int V, E, x, y, w;
    cin>>V>>E;
    for(int i=0; i<E; i++) {
        cin>>x>>y>>w;
        graph.push_back(make_pair(make_pair(x, y), w));
    }
    bool cycle = bellman_ford(0, graph, V);
    if(!cycle) {
        puts("Here is a negative cycle");
    }
    for(int i=0; i<V; i++)
        cout<<dis[i]<<" ";
    puts("");
}
bool relax(int u, int v, int w) {
    if(dis[v]>(w+dis[u])) {
        dis[v] = (w+dis[u]);
        parents[v] = u;
        return true;
    }
    return false;
}
bool bellman_ford(int source, vector< pair< pair<int, int>, int> >graph, int V) {
    for(int i=0; i<=V; i++) {
        dis[i] = 100;
        parents[i] = 0;
    }
    dis[source] = 0;
    bool check;
    for(int i=0; i<(V-1); i++) {
        for(auto it : graph) {
            check = relax(it.first.first, it.first.second, it.second);
        }
    }
    puts("");
    for(auto it : graph) {
        if(relax(it.first.first, it.first.second, it.second)) {
            return false;
        }
    }
    return true;
}
