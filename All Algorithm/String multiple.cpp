#include<bits/stdc++.h>
using namespace std;
char total_mult[1000];
void mult(char value1[], char value2[]);
int main()
{
    char value1[1000], value2[1000];
    while(cin>>value1>>value2){
        mult(value1, value2);
        printf("%s\n",total_mult);
    }
    return 0;
}
void mult(char value1[], char value2[])
{
    int i, j, k, l, mult, len, len2, small_sum;

        len=strlen(value1);
        len2=strlen(value2);
        char sum[len2+10][len*len2];
        memset(sum, 0, sizeof(sum));
        /// This is main loop for finding total multiple;
        for(i=len2-1, l=0, mult; i>=0; i--, l++){
            for(j=len-1,k=l, mult=0; j>=0; j--,k++){
                mult = mult+((value1[j]-48)*(value2[i]-48));
                sum[i][k] = mult % 10;
                mult = mult/10;
            }
            if(mult>0) sum[i][k++]=mult;
        }len=k;
        /// loop for finding total sum;
        for(i=0, small_sum=0; i<len; i++){
            for(j=0; j<len2; j++)
                small_sum=sum[j][i]+small_sum;

            total_mult[i]=(small_sum%10)+48;
            small_sum=small_sum/10;
        }
        if(small_sum>0) total_mult[i++]=small_sum+48;
        total_mult[i]=0;
        reverse(total_mult,total_mult+len);
}
