#include <bits/stdc++.h>
using namespace std;

class BST {
  private:
    struct node {
        int data;
        node *left, *right;
        node(int temp) {
            data = temp;
            left = NULL;
            right = NULL;
        }
    };
    node *root = NULL;
  public:
    node *minValueNode(struct node *_node) {
        node *current = _node;
        while (current->left != NULL)
            current = current->left;
        return current;
    }
    node *_deleteNode(node *root, int data) {
        if(root == NULL)
            return root;
        if(data < root->data)
            root->left = _deleteNode(root->left, data);
        else if(data > root->data)
            root->right = _deleteNode(root->right, data);
        else {
            if(root->left == NULL) {
                node *temp = root->right;
                free(root);
                return temp;
            } else if (root->right == NULL) {
                node *temp = root->left;
                free(root);
                return temp;
            }
            node *temp = minValueNode(root->right);
            root->data = temp->data;
            root->right = _deleteNode(root->right, temp->data);
        }
        return root;
    }

    void Insert(int data) {
        if(root==NULL) {
            root = new node(data);
            return;
        }
        node *current = root, *parent;
        while(current!=NULL) {
            parent = current;
            if(current->data<data)
                current = current->right;
            else if(current->data>data)
                current = current->left;
            else
                return;
        }
        if(parent->data<data)
            parent->right = new node(data);
        else
            parent->left = new node(data);
    }
    void _preOrder(node *current) {
        if(current==NULL)
            return;
        printf("%3d", current->data);
        _preOrder(current->left);
        _preOrder(current->right);
    }
    void _inOrder(node *current) {
        if(current==NULL)
            return;
        _inOrder(current->left);
        printf("%3d", current->data);
        _inOrder(current->right);
    }
    void _postOrder(node *current) {
        if(current==NULL)
            return;
        _postOrder(current->left);
        _postOrder(current->right);
        printf("%3d", current->data);
    }
    bool Find(int data, node *current) {
        if(current==NULL)
            return false;
        if(current->data==data)
            return true;
        if(current->data>data)
            return Find(data, current->left);
        else
            return Find(data, current->right);
    }
    void preOrder() {
        _preOrder(root);
    }
    void inOrder() {
        _inOrder(root);
    }
    void postOrder() {
        _postOrder(root);
    }
    void Find(int data) {
        Find(data, root);
    }
    void deleteNode(int data) {
        node *temp = _deleteNode(root, data);
    }
};
int main() {
    BST root;
    int arr[] = {10, 12, 9, 5, 2, 3, 11, 13, 6};
    puts("Start");
    for(int i=0; i<sizeof(arr)/sizeof(int); i++) {
        root.Insert(arr[i]);
    }
    for(int i=0; i<(sizeof(arr)/sizeof(int))-1; i++) {
        root.preOrder();
        puts("");
        root.inOrder();
        puts("");
        root.postOrder();
        puts("");
        root.deleteNode(arr[i]);
    }


    return 0;
}
