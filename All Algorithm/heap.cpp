#include <bits/stdc++.h>
using namespace std;
bool comp(int parents, int child){
    return parents<child;
}
int Parent(int i) {
    return i>>1;
}
int Left(int i) {
    return i<<1;
}
int Right(int i) {
    return (i<<1)+1;
}

void heapify(vector<int>&Array, int pos, int heap_size) {
    int l = Left(pos), r = Right(pos), largest;
    if(l<=heap_size && comp(Array[l], Array[pos])) {
        largest = l;
    } else {
        largest = pos;
    }
    if(r<=heap_size && comp(Array[r], Array[largest])) {
        largest = r;
    }
    if(largest != pos) {
        swap(Array[pos], Array[largest]);
        heapify(Array, largest, heap_size);
    }
}
void add(vector<int>&Array, int num){
    Array.push_back(num);
    int Size = Array.size();
    heapify(Array, 2, Size);
}
int main()
{
    vector<int>Array;
    add(Array, 10);
    add(Array, 2);
    add(Array, 5);
    add(Array, 1);
    for(auto it : Array){
        cout<<it<<" ";
    }
    puts("");
    return 0;
}
