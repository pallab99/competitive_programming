#include<bits/stdc++.h>
#define N 177000000
using namespace std;
int prime[10000000];

unsigned pri[(N>>6)+5];
#define Check(n) (pri[n>>6]&(1<<((n>>1)&31)))
#define Set(n) (pri[n>>6]|=(1<<((n>>1)&31)))

void bitwise_seive() {
    int i, J, root = int(sqrt(N)+2);
    for(i=3, Set(1); i<root; i+=2)if(!Check(i)){
        for(J=i*i; J<N; J+=(i<<1))Set(J);
    }
    for(prime[0]=2, i=3, J=1; i<N; i+=2)if(!Check(i)){
        prime[J++]=i;
    }
    cout<<clock()<<endl;
}

inline bool big_prime(long long n) {
    if(n==2) return true;
    if((n&1)==0) return false;
    if(n<177000000) return !Check(n);
    long long J, root=(int)sqrt(n);
    for(J=0; prime[J]<=root; J++) {
        if(n%prime[J]==0) {
            return false;
        }
    }
    return true;
}
int main() {
    bitwise_seive();
    long long n, root, i;
    while(cin>>n){
        cout<<pri[n]<<endl;
        if(big_prime(n)==true)printf("Yes\n");
        else printf("No\n");
    }
    return 0;
}
