#include <stdio.h>
int A[50][50], B[50][50], ans[50][50];
int main()
{
    int n, m, x, y, num, i, j, k, sum;
    puts("Input size of matrix A (n & m)");
    scanf("%d%d", &n, &m);
    puts("Input matrix A");
    for(i=0; i<n; i++)for(j=0; j<m; scanf("%d", &A[i][j++]));
    puts("Input size of matrix B (x & y)");
    scanf("%d%d", &x, &y);
    puts("Input matrix A");
    for(i=0; i<x; i++)for(j=0; j<y; scanf("%d", &B[i][j++]));
    if(m!=x) return puts("Not possible"), 0;
    puts("Ans is\n");
    for(i=0; i<n; i++)for(j=0; j<y; j++){
        for(k=0, sum=0; k<m; sum+=(A[i][k]*B[k][j]), k++);
        ans[i][j]=sum;
    }
    for(i=0; i<n; i++){for(j=0; j<y; printf("%4d", ans[i][j++]));puts("");}
    return 0;
}
