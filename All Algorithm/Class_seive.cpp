#include <bits/stdc++.h>
using namespace std;
unsigned prime[664600];
class PRIME{
    #define LMT 10000000
    unsigned pri[(LMT>>6)+5]={0};
    #define Check(n) (pri[n>>6] & (1<<((n>>1)&31)))
    #define Set(n) (pri[n>>6] |= (1<<((n>>1)&31)))
public:
    void seive(){
        int i, j, root=sqrt(LMT)+2;
        for(Set(1), i=3; i<root; i+=2)if(!Check(i)){
            for(j=i*i; j<LMT; j+=i*2) Set(j);
        }
        prime[0]=2;
        for(i=3, j=1; i<LMT; i+=2)if(!Check(i)){
            prime[j++]=i;
        }
    }
    bool isPrime(int n) {
        if(n==2) return 1;
        if(!(n&1)) return 0;
        return !Check(n);
    }
};
int main()
{
    PRIME P;
    P.seive();
    int i, j;
    while(cin>>i){
        if(P.isPrime(i)) puts("Yes");
        else puts("No");
    }
    cout<<clock()<<endl;
}
