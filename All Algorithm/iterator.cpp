#include <bits/stdc++.h>
using namespace std;
int i;
void MAP() {
    map<string, int>M;
    M["asdf"]=50; M["qwer"]=100; M["ghjf"]=76;
    map<string, int>::iterator it;
    for(it=M.begin(); it!=M.end(); it++){
        cout<<(it->first)<<" "<<(it->second)<<endl;
        /// Here first mean position and second mean value.
    }
}
void LIST(){
     // constructors used in the same order as described above:
    list<int> first;                                 // empty list of ints
    list<int> second (4,100);                        // four ints with value 100
    list<int> third (second.begin(),second.end());   // iterating through second
    list<int> fourth (third);                        // a copy of third
    // the iterator constructor can also be used to construct from arrays:
    int myints[] = {16,2,77,29};
    list<int> fifth (myints, myints + sizeof(myints) / sizeof(int) );
    for(list<int>::iterator it = fifth.begin(); it != fifth.end(); it++)
        cout<<*it<<' ';
    cout<<endl;
}
int main()
{
    ///MAP();
    LIST();
    return 0;
}
