#include <iostream>
using namespace std;
class abc{
    int a;
public:
    abc(int x){ a = x; }
    abc(int x, int y){ a = x + y; }
    abc(int x, int y, int z){ a = x*y*z; }
    int ret(){ return a; }
};

int main()
{
    abc obj(5);
    abc obj1(5,6);
    abc obj2(5, 6, 7);

    cout<< obj.ret() <<endl;
    cout<< obj1.ret() <<endl;
    cout<< obj2.ret() <<endl;

    return 0;
}
