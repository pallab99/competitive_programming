#include <bits/stdc++.h>
using namespace std;
int KMP(string test, string pattern) {
    int i, j, len, len1;
    len = pattern.size();
    len1 = test.size();
    if(len>len1) return -1;
    int temp[len];
    for(i=1, j=0, temp[0]=0; i<len; ){
        // if index i and index j is same then increment i & j;
        if(pattern[i]==pattern[j]){temp[i++] = ++j;}
        // if j is in index 0 then forward i;
        else if(j==0) temp[i++]=0;
        // if index i and index j is not same then decrement j;
        else j=temp[j-1];
    }
    for(i=0, j=0; i<len1 && j<len; ) {
        if(pattern[j]==test[i]){i++, j++;} // flow past loop commend;
        else if(j==0) i++;
        else j=temp[j-1];
    }
    if(j==len) return i-len;
    return -1;
}
int main()
{
    string text, pattern;
    while(cin>>text>>pattern) {
        cout<<KMP(text, pattern)<<endl;
    }
}
