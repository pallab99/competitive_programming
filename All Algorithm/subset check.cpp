#include <bits/stdc++.h>
using namespace std;
inline bool cmp(vector<int>a, vector<int>b) {
    return a.size()<b.size();
}
vector< vector<int> > getAllSubsets(vector<int> Set)
{
    vector< vector<int> > subset;
    vector<int> empty;
    subset.push_back(empty);
    int i, j, len, len1;
    for(i=0; i<Set.size(); i++){
        vector< vector<int> > subsetTemp = subset;
        for(j=0, len=subsetTemp.size(); j<len; j++)
            subsetTemp[j].push_back( Set[i] );
        for(j=0, len1=subsetTemp.size(); j<len1; j++)
            subset.push_back( subsetTemp[j] );
    }
    return subset;
}
int main()
{
    vector< vector<int> >subset;
    vector< int >Set;
    int i, j, len, len1, x, n, ans;
    while(cin>>n) {
        Set.clear();
        subset.clear();
        for(i=0; i<n; i++) {
            scanf("%d", &x);
            Set.push_back(x);
        }
        subset = getAllSubsets(Set);
        sort(subset.begin(), subset.end(), cmp);
        ans = 0;
        for(i=1, len=subset.size(); i<len; i++) {
            for(j=0, len1=subset[i].size(); j<len1; j++)
                printf("%d ", subset[i][j]);
            puts("");
            ans++;
        }
        cout<<ans<<endl;
    }
    return 0;
}
