#include <cstdio>
#include <iostream>
#include <math.h>
using namespace std;
double call(int x, int n) {
	double lo=0.0, hi=x, mid;
	for(int i=0; i<64; i++){
		mid = (lo+hi)/2;
		if(pow(mid,n)>x)
			hi=mid;
		else
			lo = mid;
	}
	return mid;
}
int main()
{
    double N, n, mid;
    while(cin>>n>>N){ // here N is number and n is number root;
        mid = call(N, n);// like n root N;
        cout<<mid<<endl;
    }
	return 0;
}
