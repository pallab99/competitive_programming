#include <bits/stdc++.h>
#define SIZ 100
using namespace std;
bool visit[SIZ];
bool aci[SIZ];
vector<int>ans, V[SIZ];
void TSort(int node) {
    if(!visit[node]){
        int i, len = V[node].size();
        for(i=0; i<len; i++){
            TSort(V[node][i]);
        }
        visit[node]=1;
        ans.push_back(node);
    }
}
int main()
{
    freopen("graph.txt", "r", stdin);
    int i, x, y, node, edge, len;
    cin>>node>>edge;
    while(edge--){
        scanf("%d%d", &x, &y);
        V[x].push_back(y);
        aci[x]=1;
    }
    for(i=1; i<=node; i++){
        TSort(i);
    }
    for(i=0, len=ans.size(); i<len; i++){
        printf("%d ", ans[i]);
    }puts("");
    return 0;
}
/** 11 6 2 4 5 3 1 */
