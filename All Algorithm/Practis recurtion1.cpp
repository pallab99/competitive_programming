#include <iostream>
#include <cstdio>
using namespace std;

void input(int arr[],int n, int pos)
{
    if(pos==n) return;
    cin>>arr[pos];
    return input(arr, n, pos+1);
}
void print(int arr[], int n, int pos)
{
    if(pos==n) return;
    cout<<arr[pos]<<' ';
    return print(arr, n, ++pos);
}
int main()
{
    int arr[100], n;
    cin>>n;
    input(arr, n, 0);
    print(arr, n, 0);
    cout<<endl;
    return 0;
}
