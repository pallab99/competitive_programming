#include <bits/stdc++.h>
using namespace std;
string LCS(string s, string sub) {
    int len, len1, i, j, pos, pos1, Max=0;
    len = s.size();  len1 = sub.size();
    int ans[len1+5][len+5]={0};
    for(i=0; (i<=len || i<=len1); i++){
        if(i<=len) ans[0][i]=0;
        if(i<=len1) ans[i][0]=0;
    }
    for(i=0; i<len1; i++) {
        for(j=0; j<len; j++) {
            if(sub[i]==s[j]){
                ans[i+1][j+1] = ans[i][j]+1;
                if(Max<ans[i+1][j+1]) {
                    Max = ans[i+1][j+1];
                    pos=i+1, pos1=j+1;
                }
            }
            else{
                ans[i+1][j+1] = 0;
            }
        }
    }

    printf("%d\n",ans[pos][pos1]);
    //for(i=0; i<=len1; i++) {for(j=0; j<=len; j++)printf("%d ", ans[i][j]);puts("");}
    sub.clear();
    while(ans[pos][pos1]) {
        sub+=s[pos1-1];
        pos1--, pos--;
    }
    reverse(sub.begin(), sub.end());
    return sub;
}
int main()
{
    freopen("input.txt", "r", stdin);
    string s, sub, siq;
    while(cin>>s>>sub) {
        siq = LCS(s, sub);
        cout<<siq<<endl;
    }
    return 0;
}

