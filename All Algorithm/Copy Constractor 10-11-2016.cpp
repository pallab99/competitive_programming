#include <iostream>
using namespace std;

class abc{
    int *p;
public:
    abc(){p = new int(0); cout<<"cons..."<<*p<<endl;}

    ~abc(){cout<<"dis..."<<endl; delete p;}

    void set(int x){*p = x;}

    void get(){ cout<<*p<<endl;}

    abc(abc &obj);
};
abc::abc(abc &a)
{
    p = new int(1);
    cout<<"cons...  "<<*p<<endl;
}
int main()
{
    abc a;
    a.get();
    abc b = a;
    a.get();
    b.get();
    b.set(4);
    a.get();
    b.get();

    return 0;
}
