#include <bits/stdc++.h>
#define SIZ 100
#define MAX 100000
using namespace std;
vector<int>edge[SIZ], cost[SIZ];
int dis[SIZ];

void BFS_SP(int source);
int main()
{
    freopen("graph.txt", "r", stdin);
    int x, y, w, source, edg;
    cin>>edg;
    while(edg--){
        cin>>x>>y>>w;
        edge[x].push_back(y);
        edge[y].push_back(x);
        cost[x].push_back(w);
        cost[y].push_back(w);
    }
    cin>>source>>edg;
    BFS_SP(source);
    cout<<dis[edg]<<endl;
    return 0;
}
void BFS_SP(int source) {
    int v, vcost, u, ucost, i, len;
    for(i=1; i<100; dis[i++]=MAX);
    queue<int> q;
    q.push(source);
    dis[source] = 0;
    while(!q.empty()) {
        u = q.front(); q.pop();
        len = edge[u].size();
        ucost = dis[u];
        for(i=0; i<len; i++) {
            v = edge[u][i], vcost = cost[u][i]+ucost;
            if(dis[v]>vcost){
                dis[v]=vcost;
                q.push(v);
            }
        }
    }
}
/**
12
1 4 6
1 3 4
1 2 2
2 6 2
2 4 4
3 4 1
3 5 3
4 6 1
4 7 3
4 5 1
5 7 2
6 7 9
1 5
*/
