#include <bits/stdc++.h>
using namespace std;
class Comp {
  public:
    bool operator()(pair<long long, int> A, pair<long long, int> B) {
        return A.first>B.first;
    }
};
class dijkstra {
  private:
    int Size, source, MAXI, inc;
    bool RUN;
    vector< vector< pair<int, int> > > graph;
    vector<int> Path;
    vector<long long> distance;
    vector<bool>mark;
  public:
    dijkstra(int s) {
        RUN = false, Size = s, MAXI = INT_MAX, inc = -1;
        graph.resize(Size+1);
        Path.resize(Size+1);
        distance.resize(Size+1);
        mark.resize(Size+1);
    }

    void add(int from, int to, int weight) {
        //from += inc, to += inc;
        graph[from].push_back(make_pair(weight, to));
        RUN = false;
    }
    void showTheGroup(){
        int s = 0;
        for(auto it : graph){
            printf("%d", s++);
            for(auto it1 : it){
                printf(" -> (%d,%d)", it1.second, it1.first);
            }
            puts("");
        }
    }
    vector<int> path(int dis) {
        vector<int>p;
        if(RUN==false) {
            return p;
        }
        int past = dis;
        p.push_back(past);
        while(past != source && Path[past] != source){
            past = Path[past];
            p.push_back(past);
        }
        p.push_back(source);
        reverse(p.begin(), p.end());
        return p;
    }
    void Clear() {
        graph.clear();
        Path.clear();
        distance.clear();
        mark.clear();
    }
    void run(int S) {
        pair<long long, int> parents;
        priority_queue<pair<long long, int>, vector<pair<long long, int>>, greater<pair<long long, int>> >Q;
        //priority_queue<pair<int, int>, vector<pair<int, int>>, Comp >Q;

        fill(mark.begin(), mark.end(), false);
        fill(distance.begin(), distance.end(), MAXI);

        source = S;
        distance[source] = 0;
        Q.push(make_pair(0, source));
        while(!Q.empty()) {
            parents = Q.top();
            Q.pop();
            if(mark[parents.second]==false) {
                for(auto now : graph[parents.second]) {
                    now.first += parents.first;
                    if(distance[now.second]>now.first) {
                        Path[now.second] = parents.second;
                        distance[now.second] = now.first;
                        Q.push(now);
                    }
                }
            }
            mark[parents.second] = true;
        }
        RUN = true;
    }
};
int main() {
    int n, m, x, y, w;
    cin>>n>>m;
    dijkstra D(n);
    for(int i=0; i<m; i++){
        scanf("%d%d%d", &x, &y, &w);
        D.add(x, y, w);
        D.add(y, x, w);
    }
    D.run(1);
    vector<int>path = D.path(n);
    for(auto it : path){
        printf("%d ", it);
    }
}

