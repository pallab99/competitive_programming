#include <iostream>
#include <vector>
#include <algorithm>
#include <cstdio>
using namespace std;
int main()
{
    vector<int> v;
    v.push_back(2); v.push_back(3); v.push_back(4);
    v.push_back(7); v.push_back(1); v.push_back(10);
    vector<int>::iterator i;
    for(i = v.begin(); i != v.end(); i++)
        printf("%d ",*i);
    printf("\n");
    sort(v.begin(),v.end());
    for(i = v.begin(); i != v.end(); i++)
        printf("%d ",*i);
    printf("\n");
    return 0;
}
