#include<bits/stdc++.h>
using namespace std;
const int MX = 100;
vector <int>g[MX+10];
int dis[MX], to;
bool vis[MX+10];
int BFS(int i);
int main()
{
    freopen("graph.txt", "r", stdin);
    int node, edge, x, y, from;
    scanf("%d%d",&node,&edge);
    while(edge--){
        scanf("%d%d",&x,&y);
        g[x].push_back(y);
        g[y].push_back(x);
    }
    while(cin>>from>>to){
        memset(dis, 0, sizeof(dis));
        memset(vis, 0, sizeof(vis));
        cout<<from<<" to "<<to<<" Distance is "<<BFS(from)<<endl;
    }
    return 0;
}
int BFS(int i){
    int len, J;
    vis[i] = 1;
    queue<int>Q;
    Q.push(i);
    while(!Q.empty()){
        i = Q.front();
        len = g[i].size();
        Q.pop();
        for(J=0; J<len; ++J){
            if(!vis[g[i][J]]){
                vis[g[i][J]] = 1;
                dis[g[i][J]] = dis[i]+1;
                Q.push(g[i][J]);
            }
        }
    }
}
