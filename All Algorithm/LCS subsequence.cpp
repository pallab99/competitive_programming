#include <bits/stdc++.h>
using namespace std;
string LCS(string s, string sub) {
    int len, len1, i, j;
    len = s.size();  len1 = sub.size();
    int ans[len1+5][len+5];
    for(i=0; (i<=len || i<=len1); i++){
        if(i<=len) ans[0][i]=0;
        if(i<=len1) ans[i][0]=0;
    }
    for(i=0; i<len1; i++) {
        for(j=0; j<len; j++) {
            if(sub[i]==s[j]){
                ans[i+1][j+1] = ans[i][j]+1;
            }
            else{
                ans[i+1][j+1] = max(ans[i+1][j], ans[i][j+1]);
            }
        }
    }
    cout<<ans[len1][len]<<endl;
    //for(i=0; i<=len1; i++) {for(j=0; j<=len; j++)printf("%d ", ans[i][j]);puts("");}
    sub.clear();
    while(len!=0 && len1!=0) {
        i = ans[len1][len-1]; j = ans[len1-1][len];
        if((i==j) && (ans[len1-1][len-1]<ans[len1][len])) {
            sub+=s[len-1];
            len1--; len--;
        }
        else if(i==ans[len1][len]) len--;
        else len1--;
    }
    reverse(sub.begin(), sub.end());
    return sub;
}
int main()
{
    //freopen("input.txt", "r", stdin);
    string s, sub, siq;
    while(cin>>s>>sub) {
        siq = LCS(s, sub);
        cout<<siq<<endl;
    }
    return 0;
}
