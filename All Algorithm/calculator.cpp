#include<bits/stdc++.h>
using namespace std;
#define mod(a, b) a-(int)(a/b)*b
class Calculator {
    bool is_number(char c) {
        return c>='0' && c<='9';
    }
    double calculate(double a, double b, char c) {
        if(c=='^')
            return pow(a, b);
        else if(c=='%') {
            if(b==0)
                return a;
            return mod(a, b);
        } else if(c=='/') {
            if(b==0)
                return 0;
            return a/b;
        } else if(c=='*')
            return a*b;
        else if(c=='+')
            return a+b;
        else if(c=='-')
            return a-b;
    }
    double to_number(string s) {
        double sum=0;
        for(auto it : s)
            sum = sum*10+(it-'0');
        return sum;
    }
    double postfix_to_cal(vector<string>postfix) {
        stack<double>S;
        double a, b;
        char c;
        for(auto it : postfix) {
            if(is_number(it[0]))
                S.push(to_number(it));
            else {
                b = S.top();
                S.pop();
                a = S.top();
                S.pop();
                S.push(calculate(a, b, it[0]));
            }
        }
        return S.top();
    }
    vector<string> to_postfix(string expre) {
        map<char, int>precidency;
        precidency['('] = 0;
        precidency['+'] = 1, precidency['-'] = 1;
        precidency['*'] = 2;
        precidency['/'] = 3;
        precidency['%'] = 4;
        precidency['^'] = 5;
        string temp = "(";
        int len = expre.size();
        for(int i=0; i<len; i++) {
            if(expre[i]!=' ') {
                if(temp.back()==')' && is_number(expre[i])) {
                    temp += "*";
                } else if(is_number(temp.back()) && expre[i]=='(') {
                    temp += "*";
                }
                temp+=expre[i];
            }
        }
        expre = temp+")";
        stack<char>Stk;
        len = expre.size();
        vector<string>postfix;
        for(int i=0; i<len; i++) {
            if(is_number(expre[i])) {
                temp = "";
                for(; is_number(expre[i]); i++) {
                    temp+=expre[i];
                }
                postfix.push_back(temp);
                i--;
            } else {
                char c = expre[i];
                if(c=='(')
                    Stk.push(c);
                else if(c==')') {
                    while(Stk.top()!='(') {
                        string temp = "";
                        temp+=Stk.top();
                        postfix.push_back(temp);
                        Stk.pop();
                    }
                    Stk.pop();
                } else if(precidency[Stk.top()]<precidency[c]) {
                    Stk.push(c);
                } else {
                    while(Stk.top()!='(' && precidency[Stk.top()]>precidency[c]) {
                        string temp = "";
                        temp+=Stk.top();
                        postfix.push_back(temp);
                        Stk.pop();
                    }
                    Stk.push(c);
                }
            }
        }
        return postfix;
    }
public:
    double result(string str) {
        return postfix_to_cal(to_postfix(str));
    }
};
int main() {
    Calculator C;
    string str = "6*(5/2*3)";
    cout<<C.result(str)<<endl;
    return 0;
}
