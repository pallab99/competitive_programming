#include<bits/stdc++.h>
using namespace std;
const int MX = 100;
vector <int>g[MX+10];
bool vis[MX+10];
void dfs(int i);
int main()
{
    freopen("input.txt", "r", stdin);
    int node, edge, x, y;
    scanf("%d%d",&node,&edge);
    while(edge--){
        scanf("%d%d",&x,&y);
        g[x].push_back(y);
        g[y].push_back(x);
    }
    int connected=0;
    for(int i=1;i<=node; ++i){
        if(!vis[i]){
            dfs(i);
            connected++;
        }
    }
    printf("Here %d connected Graph\n", connected);
    return 0;
}
void dfs(int i) {
    vis[i] = 1;
    int len= g[i].size(), J;
    for(J=0; J<len; ++J){
        if(!vis[g[i][J]]){
            vis[g[i][J]]=1;
            dfs(g[i][J]);
        }
    }
}
