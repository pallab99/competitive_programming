#include <iostream>
using namespace std;
int queen[20], column[20], diagonal1[40], diagonal2[40];

void nqueen(int at, int n);
int main()
{
    nqueen(8,8);
    return 0;
}
void nqueen(int at, int n)
{
    if(at == n+1){
        cout<<"(row, column) = ";
        for(int i=1; i<=n; i++)
            cout<<i<<", "<<queen[i];
        cout<<endl;
        return;
    }
    for(int i=1; i<=n; i++){
        if(column[i] || diagonal1[n+1] || diagonal2[ n+ i - at ])
            continue;

        queen[at] = i;

        column[i] = diagonal1[i+at] = diagonal2[n+i-at] = 1;
        nqueen(at+1,n);
        column[i] = diagonal1[i+at] = diagonal2[n+i-at] = 0;
    }
}
