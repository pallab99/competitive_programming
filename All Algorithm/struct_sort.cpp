#include "bits/stdc++.h"
using namespace std;
struct Range
{
    int st,end,minval;
    Range(){}
    Range(int a,int b,int c){
        st = a;
        end = b;
        minval = c;
    }
};
bool compare(Range a, Range b){
    if(a.end-a.st>b.end-b.st) return 0;
    return 1;
}
int main()
{
    struct Range inputs[4];
    int x, y, z, n;
    cin>>n;
    for(int i=0; i<n; i++){
        cin>>x>>y>>z;
        inputs[i] = Range(x,y,z);
    }
    sort(inputs,inputs+n,compare);
    for(int i=0; i<n; i++){
        cout<<inputs[i].st<<" "<<inputs[i].end;
        cout<<" "<<inputs[i].minval<<endl;
    }
    return 0;
}
