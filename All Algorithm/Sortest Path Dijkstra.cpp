#include <bits/stdc++.h>
#define SIZ 100
#define MAX 100000
using namespace std;
struct node{
    int city, dist;
    inline bool operator<(const node& p)const {
        //printf("dist %4d p.dist %4d\n",dist, p.dist);
        return dist > p.dist;
    }
}u, v;

vector<int>edge[SIZ], cost[SIZ];
int dis[SIZ], arr[SIZ], L;

void Dijkstra_SP(int source);
void BFS_SP(int source);
int main()
{
    freopen("graph.txt", "r", stdin);
    int x, y, w, sorch, to_city, edg;
    cin>>edg;
    while(edg--){
        scanf("%d%d%d", &x, &y, &w);
        edge[x].push_back(y);
        edge[y].push_back(x);
        cost[x].push_back(w);
        cost[y].push_back(w);
    }
    cin>>sorch>>to_city;
    L=0;
    Dijkstra_SP(sorch);
    BFS_SP(sorch);
    printf("\n%d\n", dis[to_city]);
}
void Dijkstra_SP(int source) {
    int len, i, ucost;
    for(i=0; i<100; dis[i++]=MAX);
    puts("\n\tDijkstra_SP\n");
    priority_queue<node>q;
    u.city = source, u.dist = 0;
    q.push(u);
    dis[source] = 0;
    while(!q.empty()) {

        u = q.top(); q.pop();
        ucost = dis[u.city];
        len = edge[u.city].size();

        for(i=0; i<len; i++) {
            v.city = edge[u.city][i], v.dist = cost[u.city][i] + ucost;
            if(dis[v.city] > v.dist) {
                dis[v.city] = v.dist;
                q.push(v);
                printf("%3d %3d\n", v.city, v.dist);
            }
        }
        printf("Queue size is %d\n", q.size());
    }
}
void BFS_SP(int source) {
    int v, vcost, u, ucost, i, len;
    for(i=1; i<100; dis[i++]=MAX);
    puts("\n\tBFS_SP\n");
    queue<int> q;
    q.push(source);
    dis[source] = 0;
    while(!q.empty()) {
        u = q.front(); q.pop();
        len = edge[u].size();
        ucost = dis[u];
        for(i=0; i<len; i++) {
            v = edge[u][i], vcost = cost[u][i]+ucost;
            if(dis[v]>vcost){
                dis[v]=vcost;
                q.push(v);
                printf("%3d %3d\n", v, vcost);
            }
        }
        printf("Queue size is %d\n", q.size());
    }
}
/**
12
1 4 6
1 3 4
1 2 5
2 6 2
2 4 4
3 4 7
3 5 3
4 6 6
4 7 3
4 5 4
5 7 2
6 7 9
1 5


*/
