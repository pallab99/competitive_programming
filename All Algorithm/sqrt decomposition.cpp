#include <bits/stdc++.h>
using namespace std;
int arr[10000];
vector<int> block;
int blk_sz;
// Complexity : O(1)
void update(int idx, int val)
{
    int blockNumber = idx / blk_sz;
    block[blockNumber] += val - arr[idx];
    arr[idx] = val;
}
// Complexity : O(sqrt(n))
int query(int l, int r)
{
    int sum = 0;
    for (; l < r and l % blk_sz != 0 and l != 0; l++)
    {
        sum += arr[l];
    }
    for (; l + blk_sz <= r; l += blk_sz)
    {
        sum += block[l / blk_sz];
    }
    for (; l <= r; l++)
    {
        sum += arr[l];
    }
    return sum;
}
void construct(int input[], int n)
{
    int blk_idx = -1;
    blk_sz = sqrt(n);
    block.resize(blk_sz + 1);
    for (int i = 0; i < n; i++)
    {
        arr[i] = input[i];
        block[i / blk_sz] += input[i];
    }
}

int main()
{
    int input[] = {1, 5, 2, 4, 6, 1, 3, 5, 7, 10};
    int n = sizeof(input) / sizeof(input[0]);

    construct(input, n);

    cout << "query(3,8) : " << query(3, 8) << endl; // 26
    cout << "query(1,6) : " << query(1, 6) << endl; // 21
    update(8, 0);
    cout << "query(8,8) : " << query(8, 8) << endl; // 0
    return 0;
}
