#include <bits/stdc++.h>
using namespace std;
#define Size 100
vector<int>graph[Size], graphRev[Size], component[Size];
stack<int> Stack;
bool mark[Size], visited[Size];
int many;
void print(vector<int>V[], int len){
    for(int i=0; i<len; i++){
        for(auto x : V[i])cout<<x+1<<' '; cout<<endl;
    }
}
void dfs(int node){
    mark[node] = 1;
    for(auto v : graph[node])if(!mark[v]) dfs(v);
    Stack.push(node);
}
void dfs1(int node){
    mark[node] = 1;
    for(auto v : graphRev[node])if(!mark[v]) dfs1(v);
    component[many].push_back(node);
}
void rev(vector<int> G[], int len){
    for(int i=0; i<len; i++)for(auto x : graph[i])G[x].push_back(i);
}
void findSCC(int node){
    memset(mark, 0, sizeof(mark));
    for(int i=0; i<node; i++)if(!mark[i]) dfs(i);
    rev(graphRev, node);
    memset(mark, 0, sizeof(mark));
    many=0;
    while(!Stack.empty()){
        if(!mark[Stack.top()]) dfs1(Stack.top()), many++;
        Stack.pop();
    }
    print(component, many);
}
int main()
{
    freopen("input.txt", "r", stdin);
    int x, y, edge, node;
    cin>>node>>edge;
    for(int i=0; i<edge; i++){
        scanf("%d%d", &x, &y);
        x--, y--;
        graph[x].push_back(y);
    }
    findSCC(node);
}
