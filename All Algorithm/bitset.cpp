#include <iostream>
#include <bitset>
#define LMT 10000000
using namespace std;
bitset<LMT> bit[LMT];

int main()
{
    cout<<sizeof(bit)<<endl;
    for(int i=0; i<10; i++){
        cout<<bit[i]<<endl;
    }
    return 0;
}
